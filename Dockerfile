FROM eboraas/debian:stable

MAINTAINER Rob Green <greenr@bgsu.edu>

RUN apt-get update && \
    apt-get -y install apache2 git curl php5 php5-mcrypt php5-json php5-sqlite git && \
    apt-get -y autoremove && apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV APACHE_RUN_USER www-data 
ENV APACHE_RUN_GROUP www-data 
ENV APACHE_LOG_DIR /var/log/apache2

RUN /usr/sbin/a2ensite default-ssl
RUN /usr/sbin/a2enmod ssl
RUN /usr/sbin/a2enmod rewrite
ADD 000-laravel.conf /etc/apache2/sites-available/
ADD 001-laravel-ssl.conf /etc/apache2/sites-available/

RUN /usr/sbin/a2dismod 'mpm_*' && /usr/sbin/a2enmod mpm_prefork
RUN /usr/sbin/a2dissite '*' && /usr/sbin/a2ensite 000-laravel 001-laravel-ssl
RUN /usr/bin/curl -sS https://getcomposer.org/installer |/usr/bin/php
RUN /bin/mv composer.phar /usr/local/bin/composer

ADD ./app /var/www/laravel
RUN touch app/database/database.sqlite
RUN cd /var/www/laravel && composer update --no-scripts && composer install
RUN chmod +x artisan && php artisan key:generate && php artisan migrate && php artisan db:seed

RUN /bin/chown www-data:www-data -R /var/www/laravel/storage /var/www/laravel/bootstrap/cache /var/www/laravel/database

EXPOSE 80
EXPOSE 443

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
