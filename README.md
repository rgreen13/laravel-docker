This project is an example project that combines Docker with Laravel and SQLite for deployment via a container service like [cycle.io](http://cycle.io). In the future, the Debian base should be moved to Alpine Linux to save space. 

*This all assumes that you have:*
1. Docker properly installed and 
2. A DockerHub account.

# Building the image 

1. Clone https://gitlab.com/rgreen13/laravel-docker
2. Delete the subdirectory called TaskList
3. Replace it with a folder containing your project
4. Edit the Docker file on line 20 so that "./taskList" is changed to the name of your project
5. Modify your project to use SQLite as the database
6. Open a command line environment (Do not use Git Bash!) and change to the directory where you cloned this project
7. Run "docker build -t <DockerHubUsername>/<projectName>". As an example, I used the command "docker build -t rgreen13/laravel ."

# Moving to Dockerhub
1. Once the build completes, run "docker login" to login to your DockerHub account
2. Run "docker push "<DockerHubUsername>/<projectName>". For example, I used "docker push rgreen13/laravel".

# Deploying on Cycle.io

## Import your image

1. Log into Cycle.io
2. Click on "Images"
3. Click "Import Docker Hub Image"
4. In the "Image" textbox, enter the "<DockerHubUsername>/<projectName>" that you used above. I used "rgreen13/laravel"
5. Click "Import Image"

## Create your environment
1. Once the import is complete, click "Environments".
2. Click "Create Environment"
3. Name your environment as you see fit and click "Create Environment"
4. You will be taking back to a list of all of your active environments. 

## Add your image to the Environment
1. From the "Environments" tab, click on the environment you created above.
2. Click "Create Container"
3. Name your container "web"
4. Choose the "128MB" plan
5. Under scaling, choose "CHI 1"
6. Click "Create Container"

## Starting your container
1. Navigate to "Environments" and click on the container named "web"
2. Click the "Play" button in the upper right hand of your screen
3. Once the "Public IP" is listed, navigate to it and you can see your project!


